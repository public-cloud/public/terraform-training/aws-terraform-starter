variable "environment_name" {
  type = string
  description = "The name of the environment. Examples: development, test, stage, production"
}

variable "project_name" {
  type = string
  description = "The name of the project."
}

variable "resource_version" {
  type = string
  description = "The of the resource or application."
}
