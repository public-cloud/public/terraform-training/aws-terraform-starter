# aws-terraform-starter

This repo is used for demos and as a starter repo for those looking to get started with Terraform in AWS. It is an example of a flat simple Terraform repo. For an example of a more advance modular repo see (Link Coming Soon!).


**Note:** this module is for training or demo purposes only and has been simplified for this purpose. An s3 bucket that will really be used should have additional configuration such as encryption and turning off public access.

## Create a bucket in s3

- Log into the AWS console.
- Ensure you are in the region where the bucket should be created
- Select the AWS CloudShell icon from the top ribbon in the AWS console

![image.png](./image.png)

- Wait for the shell to load and the command prompt to appear.
- Install Terraform in the shell. Below is an example script, however, there may be newer versions of Terraform available. Check the [release page](https://releases.hashicorp.com/terraform) for the latest version.

```
wget https://releases.hashicorp.com/terraform/1.1.3/terraform_1.1.3_linux_amd64.zip
unzip terraform_1.1.3_linux_amd64.zip
mkdir ~/bin
mv terraform ~/bin
```

- Clone this repo into AWS CloudShell by running the following command by running  `git clone https://git.doit.wisc.edu/public-cloud/public/terraform-training/aws-terraform-starter.git` in the AWS CloudShell command line
- Run the `ls` command in the AWS CloudShell command line and verify there is a folder called **aws-terraform-starter**.
- Change directories into the folder by running `cd aws-terraform-starter`in the AWS CloudShell command line
- [Initialize a working directory](https://www.terraform.io/cli/commands/init) containing Terraform configuration files by running `terraform init` in AWS CloudShell command line
- [Create an execution plan](https://www.terraform.io/cli/commands/plan) by running `terraform plan` in AWS CloudShell command line
- [Execute the actions proposed](https://www.terraform.io/cli/commands/apply) in the Terraform plan by running `terraform apply` in AWS CloudShell command line
  - Terraform again outputs a plan of the resources it will create and asks for verification of the plan.
  - Enter **yes** to confirm
- Wait for the resource(s) to be created
- Verify the s3 bucket was created in the console

## Destroy the bucket
Terraform can destroy all remote objects managed by a particular Terraform configuration.
- [Show the proposed destroy changes without executing them](https://www.terraform.io/cli/commands/destroy) by running `terraform plan -destroy` in AWS CloudShell command line
- [Execute the proposed destroy changes](https://www.terraform.io/cli/commands/destroy) by running `terraform apply -destroy` in AWS CloudShell command line

