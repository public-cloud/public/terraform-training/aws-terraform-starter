
resource "random_string" "random" {
  length = 6
  special = false
  lower = true
}

# Create a bucket for image storage wit the project name and environment in the name.
resource "aws_s3_bucket" "my_first_bucket" {
  bucket = lower("${var.project_name}-${var.environment_name}-image-storage-${random_string.random.id}")

  tags = {
    Project = lower("${var.project_name}")
    Environment = lower("${var.environment_name}")
    Version = lower("${var.resource_version}")
  }
}
